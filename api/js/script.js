var hashrates = [];

	function drawChart(ip) {
		
		Highcharts.setOptions({
			time: {
				/**
				 * Use moment-timezone.js to return the timezone offset for individual
				 * timestamps, used in the X axis labels and the tooltip header.
				 */
				getTimezoneOffset: function (timestamp) {
					var zone = 'Europe/Paris', // set zone to correspond to your location.
						timezoneOffset = -moment.tz(timestamp, zone).utcOffset();

					return timezoneOffset;
				}
			}
		});

		$('#container').highcharts({
			chart: {
				type: 'spline'
			},
			title: {
				text: 'Actually connected to:' + '<b>' + ip + '</b>',
				x: -20, //text-align center
				y: 20
			},
			xAxis: {
				tickInterval: 120 * 1000, // 2 minutes interval between each date label on the graph.
				type: 'datetime',
				// definition of dateTime to show on each date label on the graph.
				dateTimeLabelFormats: {
					second: '%Y-%m-%d<br/>%H:%M:%S',
					minute: '%Y-%m-%d<br/>%H:%M',
					hour: '%Y-%m-%d<br/>%H:%M',
					day: '%Y<br/>%m-%d',
					week: '%Y<br/>%m-%d',
					month: '%Y-%m',
					year: '%Y'
				},
			},
			yAxis: {
				title: {
					text: 'Hash Rate (KH/s)'
				},
				plotLines: [{
					value: 0,
					width: 1,
					color: '#808080'
				}],
				min: 0
			},
			tooltip: {
				valueSuffix: ' KH/s'
			},
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'middle',
				borderWidth: 0
			},
			series: [
				// you can edit each gpu name.
				// you can choose each gpu color (not-set = default-color).
				{
					name: 'GPU0', // name: 'GTX1080',
					data: hashrates[0],
					// color: 'maroon'
				},
				{
					name: 'GPU1', // name: 'RX580',
					data: hashrates[1],
					// color: 'purple'
				},
				{
					name: 'GPU2',
					data: hashrates[2],
					// color: 'yellow'
				},
				{
					name: 'GPU3',
					data: hashrates[3],
					// color: ''
				},
				{
					name: 'GPU4',
					data: hashrates[4],
					// color: ''
				},
				{
					name: 'GPU5',
					data: hashrates[5],
					// color: ''
				},
				{
					name: 'GPU5',
					data: hashrates[6],
					// color: ''
				},
				{
					name: 'GPU5',
					data: hashrates[7],
					// color: ''
				}
			]
		});
	}

	function getData(ip, port) {
		if ("WebSocket" in window) {
			var ws = new WebSocket('ws://'+ip+':'+port+'/histo','text');
			var rates = [];
			for (var gpu=0; gpu<8; gpu++) {
				hashrates[gpu] = [];
				rates[gpu] = [];
			}
			ws.onmessage = function (evt) {
				var now = new Date();
				var ts = Math.round(now/1000);
				var data = evt.data.split('|');
				for (n in data) {
					var map = data[n].split(';');
					var gpu = 0;
					var uid = 0;
					var plot = {};
					for (k in map) {
						var kv = map[k].split('=');
						if (kv.length == 1)
							continue;
						if (kv[0] === 'GPU')
							gpu = parseInt(kv[1], 10);
						else if (kv[0] === 'ID')
							uid = parseInt(kv[1], 10);
						else if (kv[0] === 'TS')
							plot.timezoneOffset = parseInt(kv[1], 10);
						else if (kv[0] === 'KHS')
							plot.hashrate = parseInt(kv[1], 10) / 1000.0;
						console.log('Data received: #GPU'+gpu+': '+kv[0]+' = '+kv[1]);
					}
					if (uid == 0)
						continue;
					rates[gpu][uid] = [+new Date(plot.timezoneOffset*1000), plot.hashrate];
				}

				// sort values with id
				for (gpu in rates) {
					for (uid in rates[gpu])
						hashrates[gpu].push(rates[gpu][uid]);
				}

				drawChart(ip);
			};
			ws.onerror = function (evt) {
				var w = evt.target;
				console.log('Error! readyState=' + w.readyState); //log errors
				$('#container').html('Error! Unable to get WebSocket data from '+ip); //log errors
				return false;
			};
			ws.onclose = function() {
				// websocket is closed.
			};
		} else {
			// The browser doesn't support WebSocket
			alert("WebSocket NOT supported by your Browser!");
		}
	}

	$(function () {
		// getData('<IP>', <port>);
		getData('127.0.0.1', 4068); //get all data informations from the desire RIG IP.
	});
<!DOCTYPE HTML>
<html>
<head>
	<title>Ravencoin-Miner Web Stats Monitoring</title>
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="js/moment.min.js" type="text/javascript"></script>
	<script src="js/moment-timezone-with-data.min.js" type="text/javascript"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="refresh" content="300">
	<link href='css/style.css' rel='stylesheet' type='text/css'>
</head>
<body>
<div>
	<img id="ravencoin-main" src="img/ravencoin-main.png">
</div>

<div id="header">
	<h1>Ravencoin-Miner Web Stats Monitoring</h1>
</div>

<div id="page">
	<div id="container"></div>
</div>

<div id="footer">
	<p>&copy; 2018 <a href="https://github.com/Natizyskunk/rvnminer-api">Natizyskunk@github</a></p>
	<p>Forked from &copy; 2014-2015 <a href="https://github.com/tpruvot/ccminer">tpruvot@github</a></p>
</div>

<script src="js/script.js" type="text/javascript"></script>
</body>
</html>
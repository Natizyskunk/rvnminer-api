# Ravencoin-Miner Web Stats Monitoring
<IMG src="/Preview.png" width="850" height="450">

## DISCLAIMER.
All rights for the **'Ravencoin-Miner Web Stats Monitoring'** interface goes to **Natizyskunk** also known as user: "Le repère du mineur#1613" on the Ravencoin Community discord. 

All rights for the **'ccminer-api'** program goes to **Tpruvot** github user: "https://github.com/tpruvot". </BR>
Many many thanks to him !


## CAUTION.
the **'api'** folder need to be placed in the root folder of your miner! </BR>


## REQUIREMENTS.
- WINDOWS x32/x64 bits.
- Any CCMiner fork (ccminer, nevermore, enemy-miner, suprminer, ravencoin-miner, silent-miner, A1_min3r, A1Min3r).


## HOW-TO.
**A) Adding the RemoteManager option to the miner.** <BR />
<IMG src="/Remote-Manager-Settings.png" width="600" height="300"> <BR />
***1.*** add at the end of your batch file this option: -b <IP>:<port> <BR />
   exemple: > `-b 127.0.0.1:4068` <BR />
***2.*** You only have to do this once.

**B) Connecting to the web interface.** <BR />
<IMG src="/Preview.png" width="640" height="350"> <BR />
***1.*** Launching the miner. <BR />
***2.a.*** Go to the **'api'** folder and simply double-click on the **'websocket.htm'** file. <BR />
***2.b.*** If you prefer to use it with a PHP server you can easily do it. For this you just need to make a copy of the **rvnminer-api'** folder and place it in the **'www'** folder of your PHP server. <BR />
***3.*** Please wait just **5 seconds** and you'll see the magic happen.

**C) BONUS.** <BR />
<IMG src="/IP-Settings.png" width="640" height="380"> <BR />
***1.a.*** You can use any IP and PORT you want to manage any miner you want. <BR />
***1.b.*** To do that you just have to edit the **'websocket.htm'** file (or **'index.php'** file if your using a PHP server) and change the last line: `getData('127.0.0.1', 4068);` <BR />
***1.c.*** After that you can save and close. <BR />
***2.a.*** You can choose the number of GPU's, colors and GPU Names to see them directly on the graphical interface ;) For this you just need to edit the **'script.js'** file in the **'js'** folder. <BR />
Go at the line that start with `series: [` and edit like that: <BR />
```
series: [
  // you can edit each gpu name.
  // you can choose each gpu color (not-set = default-color).
  {
    name: 'GPU0', // name: 'GTX1080',
    data: hashrates[0],
    color: 'maroon'
  },
  {
    name: 'GPU1', // name: 'GTX1060',
    data: hashrates[1],
    color: 'purple'
  },
  etc...
]
```
***2.b.*** You are now done. <BR />
***3.*** Now close and relaunch your miner so changes can take effect. <BR />
***4.*** Go back into your web browser and refresh your web stats interface. <BR />
***5.*** Just wait a moment and you'll see the magic happen again. <BR />
***6.*** VOILA! :)


## DONATIONS.
Donations are really appreciate for the work that has been done. <BR />
You can just pay me a beer to support my GitHub projects if you want :). <BR />
**ETN:** etnkFBZBc2oD4UTnmKAZip1fHmR3ayUaWNwhcC7ijCawboEJMQrKXQUf639y7oCA1ZUod4zv59tvkMHcbh3rX3ut1DMy8PSEky <BR />
**XMR:** 49Qxq1n8djseHNpcrHsKfvDo31Hwm5iKWHMCXuCVhp61GYSKnYGfVwbDn8NvR8a9ePbFaMUKue9x8MFX9TAgQi1oUcZQoS6 <BR />
**BTC:** 19Ap5eTzST8AkmHXbdZoSJKmyztRXEy26p <BR />
**BCH:** 16LgZfMJdHYUytFysE8GCo5xXjhex7Xgir <BR />
**ETH:** 0xcd2af8a548569f5a3c289bafd12fd35fde6adcbd <BR />
**ETC:** 0x2858deda05c6845386e8a65fe87486833e0f3706 <BR />
**LTC:** Li2AjPfxbNjLdo8seXdrPvQYG8JSBy6a6e <BR />
**ZEC:** t1QAnpU3YVrCChNYSaU5G2QP4ZB4R6DmwXm <BR />
**DGE:** DLSoZccXP4ztgQTxSUsiMdSUYU8JGZzcLv <BR />
**DSH:** XfGBjovb7vG7eddpto8B5YCfRzd4qrnvDR <BR />
**EOS/PAY/CVC:** 0xcd2af8a548569f5a3c289bafd12fd35fde6adcbd <BR />
**RM:** VP5z8w3o6CkL2ZtyHKk7B4X3scfnh9FU1R <BR />
**PIRL:** 0x52c81c3D83BABe92536C2227364926aa6Ba4452c <BR />
**WAVES:** 3PDd2xzTD7aG8acMVYkcaWbM1WnKWv9kqkC <BR />


## Edited.
*This document was edited by Natizyskunk on the 09 May 2018 at 08:35 PM (GMT+1 / Paris).*
